# coding=utf8
import pymongo
import sys

client = pymongo.MongoClient('192.168.253.129', 27017)
db = client['hospital']
putian_coll = db['putians']
hospital_coll = db['hospital']
faqs_coll = db['faqs']


def get_baidu_api(n):
    baidu_api = [
        'w9Azouz6KVy40LqTLywU8lDvG2DNhclU', 'htw8jUwBbfs6AaK2MCld87j0018k28WF', 'fYhNLF4ElisejNdP1bNeA26eReezl7aN',
        'q8LO7zvyH2xyVmeNjPu3Ky1CncEGYdxE', 'YrnmGeG16qmj4Dd53vrGSMYEoHOynnPl', 'Q4XaETp2pdEjj8ASGNqhWUk2PrRXS6bG',
        'QFLGxGkBrVZIHspxo9naIPzfwP2zLd3x', 'G4zP0VI6YbBvnyF3gd78mzLKlq6Qbkws', 'qjQYceBZvY7PX0EKzZYKkP8HijLznSMy',
        'EdIUw1SDk9gOHnXudoPL2jq8shv7HVNW', 'Wmt7xu1FvgFEZtUVvWF4bL0bv2LALdkG', '3rXnWcIQYd79Tg0HSG2nn8hyipulkKOP',
        'aMOQvMbDasGOWGhvFRQTYiMDWvTWN2ed', 'ZIUuZV5mtcxP9WHMWNl4V6G4zHaNP02H', 'GHTpi5pHZFWsxg0FmoGkKvxSaEWGadRp',
        'kTkx6D982MCpUQHKL7rRvnzRcEhtvG0P', 'pCEODjZeSKVywFwOsEeu6iGPVPFp78MZ', 'HbQxOGrjGyRoyrxq02ZL1sf63CtfbGEQ',
        '651cSyWL0MW4Y2OiPYNHGCphIGw2Cbek', 'WqeYO9bvGvC4gKGyrVn2Go49d0ETmOgu'
    ]
    if n >= len(baidu_api) - 1:
        print("没有可用的api了")
        sys.exit()
        return False
    return baidu_api[n]
