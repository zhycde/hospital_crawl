# coding=utf8
from libs import faqs_coll

stopwords = [
    '问题', '问答', '生理知识', '生理知识', '检查', '诊后须知', '用药问答'
]

f = open('name_tag.txt', 'w', encoding='utf8')


def get_with_kuoaho(name):
    # 同意(
    name = name.replace('(', '（')
    name = name.replace(')', '）')
    if name.find('（') + 1 == name.find('）'):
        name = name.replace('（', '')
        name = name.replace('）', '')
    else:
        name_list = name.replace('）', '').split('（')
        name = "\t".join(name_list)
    return name


def get_without_kuoaho(name):
    if "/" in name:
        name = "\t".join(name.split("/"))
    # 认为是不同的词
    if "、" in name:
        name = "\n".join(name.split("、"))
    return name


keyword_list = []
for item in faqs_coll.find({}, {'Name': 1}):
    name = "".join(item['Name'].split())
    if '栏目' in name:
        continue
    for word in stopwords:
        name = name.replace(word, '')
    if '（' in name or '(' in name:
        ret = get_with_kuoaho(name)
    else:
        ret = get_without_kuoaho(name)
    keyword_list.append(ret)

f.write("\n".join(keyword_list))
f.close()
